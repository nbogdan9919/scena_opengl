﻿
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Window.h"
#include "Shader.hpp"
#include "Model3D.hpp"
#include "Camera.hpp"
#include "SkyBox.hpp"

#include <iostream>

const unsigned int SHADOW_WIDTH = 2048;
const unsigned int SHADOW_HEIGHT = 2048;
gps::Window myWindow;


GLfloat angle;

glm::mat4 model;

glm::mat4 view;

glm::mat4 projection;

glm::mat3 normalMatrix;
glm::mat4 lightRotation;

glm::vec3 lightDir;
glm::vec3 lightColor;

gps::Camera myCamera(
	glm::vec3(0.0f, 2.0f, 5.5f),
	glm::vec3(0.0f, 0.0f, 0.0f),
	glm::vec3(0.0f, 1.0f, 0.0f));
float cameraSpeed = 1.1f;

bool pressedKeys[1024];
float angleY = 0.0f;
GLfloat lightAngle;

gps::Model3D ground;
gps::Model3D lightCube;
gps::Model3D screenQuad;
gps::Model3D StreetLamp;
gps::Model3D plane;
gps::Model3D camel;
gps::Model3D tank;
gps::Model3D tower;
gps::Model3D heli;
gps::Model3D heli_blade;
gps::Model3D cactus;
gps::Model3D rock;
gps::Model3D desert;
gps::Model3D road;


gps::Shader lightShader;
gps::Shader screenQuadShader;
gps::Shader depthMapShader;
gps::Shader myBasicShader;
gps::Shader groundShader;

GLuint shadowMapFBO;
GLuint depthMapTexture;
GLuint modelLoc;
GLuint viewLoc;
GLuint lightDirLoc;
GLuint lightColorLoc;
GLuint projectionLoc;
GLuint normalMatrixLoc;
bool showDepthMap;

GLuint lightPositionLoc;

GLuint fogDensityLoc;
GLfloat fogDensity = 0.0f;

bool day = false;




gps::SkyBox mySkyBox;
gps::Shader skyboxShader;

std::vector<const GLchar*> faces;
glm::vec3 lightPosition = glm::vec3(0.0f, 1.0f, 0.0f);


glm::vec3 PointLightPosition[] =
{
	glm::vec3(5.0f, 10.0f, 39.0f),
	glm::vec3(-61.0f, 81.0f, 41.0f),
	glm::vec3(0.0f, 1000.0f, -50.0f),
	glm::vec3(-6.0f, 1000.0f, -100.0f)

};


glm::vec3 StreetLightPos[] =
{
	glm::vec3(-2.59f, 5.02f, 0.48f),
	glm::vec3(0.81f, 6.49f, -50.f),
	glm::vec3(-4.2f, 5.79f, -100.0f)

};
glm::vec3 StreetLightDir[] =
{
	glm::vec3(0.48f, -0.87f, -0.07f),
	glm::vec3(0.48f, -0.87f, -0.07f),
	glm::vec3(0.48f, -0.87f, -0.07f)

};
bool showDepth = false;
float daySwitch;

glm::vec4 tankPosition = glm::vec4(-2.0f, -2.0f, 80.0f,180.0f);
glm::vec4 planePosition = glm::vec4(-100.0f, 4.0f, 100.0f, 135.0f);
glm::vec4 heliPosition = glm::vec4(50.0f, 7.0f, -30.0f, 0.0f);
float planeSpeed = 2.5;
float heliSpeed = 0.5f;
float bladeRotationSpeed = 50;
float bladeRotation = 0;

float onStreetLights;
float onFlashlight;

 int viewMode = 1;



GLenum glCheckError_(const char* file, int line)
{
	GLenum errorCode;
	while ((errorCode = glGetError()) != GL_NO_ERROR) {
		std::string error;
		switch (errorCode) {
		case GL_INVALID_ENUM:
			error = "INVALID_ENUM";
			break;
		case GL_INVALID_VALUE:
			error = "INVALID_VALUE";
			break;
		case GL_INVALID_OPERATION:
			error = "INVALID_OPERATION";
			break;
		case GL_STACK_OVERFLOW:
			error = "STACK_OVERFLOW";
			break;
		case GL_STACK_UNDERFLOW:
			error = "STACK_UNDERFLOW";
			break;
		case GL_OUT_OF_MEMORY:
			error = "OUT_OF_MEMORY";
			break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			error = "INVALID_FRAMEBUFFER_OPERATION";
			break;
		}
		std::cout << error << " | " << file << " (" << line << ")" << std::endl;
	}
	return errorCode;
}
#define glCheckError() glCheckError_(__FILE__, __LINE__)



void windowResizeCallback(GLFWwindow* window, int width, int height) {
	fprintf(stdout, "window resized to width: %d , and height: %d\n", width, height);
	//TODO	
}

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (key >= 0 && key < 1024) {
		if (action == GLFW_PRESS) {
			pressedKeys[key] = true;
		}
		else if (action == GLFW_RELEASE) {
			pressedKeys[key] = false;
		}
	}
}

float horizontalAngle;
float verticalAngle;

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
	//TODO
	float width = 800;
	float height = 600;

	float mouseSpeed = 0.0005f;
	horizontalAngle += mouseSpeed * float(width / 2 - xpos);
	verticalAngle += mouseSpeed * float(height / 2 - ypos);

	myCamera.rotate(verticalAngle, horizontalAngle);
	glfwSetCursorPos(window, width / 2, height / 2);
}

void printCameraInfo()
{
	printf("Position: %f %f %f\n", myCamera.getPosition().x, myCamera.getPosition().y, myCamera.getPosition().z);
	printf("looking at:%f %f %f", myCamera.getFront().x, myCamera.getFront().y, myCamera.getFront().z);
	printf("\n\n\n");
}

void loadSkyBox()
{
	std::vector<const GLchar*> faces;
	if (day == false)
	{
		printf("night\n");
		faces.push_back("textures/skybox/night/right.png");
		faces.push_back("textures/skybox/night/left.png");
		faces.push_back("textures/skybox/night/top.png");
		faces.push_back("textures/skybox/night/bottom.png");
		faces.push_back("textures/skybox/night/front.png");
		faces.push_back("textures/skybox/night/back.png");

	}
	else {
		printf("day\n");
		faces.push_back("textures/skybox/day/right.tga");
		faces.push_back("textures/skybox/day/left.tga");
		faces.push_back("textures/skybox/day/top.tga");
		faces.push_back("textures/skybox/day/bottom.tga");
		faces.push_back("textures/skybox/day/back.tga");
		faces.push_back("textures/skybox/day/front.tga");
	}


	mySkyBox.Load(faces);

	skyboxShader.loadShader("shaders/skyboxShader.vert", "shaders/skyboxShader.frag");
	skyboxShader.useShaderProgram();




	view = myCamera.getViewMatrix();
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "view"), 1, GL_FALSE,
		glm::value_ptr(view));



	projection = glm::perspective(glm::radians(45.0f), (float)myWindow.getWindowDimensions().width / (float)myWindow.getWindowDimensions().height, 0.1f, 1000.0f);
	glUniformMatrix4fv(glGetUniformLocation(skyboxShader.shaderProgram, "projection"), 1, GL_FALSE,
		glm::value_ptr(projection));




}

void processMovement()
{
	if (pressedKeys[GLFW_KEY_Q]) {
		angleY -= 1.0f;

		view = myCamera.getViewMatrix();
		myBasicShader.useShaderProgram();
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	}

	if (pressedKeys[GLFW_KEY_E]) {
		angleY += 1.0f;

		view = myCamera.getViewMatrix();
		myBasicShader.useShaderProgram();
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	}

	if (pressedKeys[GLFW_KEY_J]) {
		lightAngle -= 1.0f;
		lightPosition.x++;
	}

	if (pressedKeys[GLFW_KEY_L]) {
		lightAngle += 1.0f;
		lightPosition.x--;
	}

	if (pressedKeys[GLFW_KEY_W]) {
		myCamera.move(gps::MOVE_FORWARD, cameraSpeed);

		view = myCamera.getViewMatrix();
		myBasicShader.useShaderProgram();
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	}

	if (pressedKeys[GLFW_KEY_S]) {
		myCamera.move(gps::MOVE_BACKWARD, cameraSpeed);

		view = myCamera.getViewMatrix();
		myBasicShader.useShaderProgram();
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	}

	if (pressedKeys[GLFW_KEY_A]) {
		myCamera.move(gps::MOVE_LEFT, cameraSpeed);

		view = myCamera.getViewMatrix();
		myBasicShader.useShaderProgram();
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	}

	if (pressedKeys[GLFW_KEY_D]) {
		myCamera.move(gps::MOVE_RIGHT, cameraSpeed);

		view = myCamera.getViewMatrix();
		myBasicShader.useShaderProgram();
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	}

	if (pressedKeys[GLFW_KEY_M]) {

		if (pressedKeys[GLFW_KEY_1])
		{
			showDepth = true;
		}
		if (pressedKeys[GLFW_KEY_2])
		{
			showDepth = false;
		}
	}
	if (pressedKeys[GLFW_KEY_N]) {

		if (pressedKeys[GLFW_KEY_1])
		{
			myBasicShader.useShaderProgram();
			GLfloat  dayNight = glGetUniformLocation(myBasicShader.shaderProgram, "dayMode");
			glUniform1f(dayNight, 1.0f);

			day = true;
			loadSkyBox();
		}
		if (pressedKeys[GLFW_KEY_2])
		{
			myBasicShader.useShaderProgram();
			GLfloat  dayNight = glGetUniformLocation(myBasicShader.shaderProgram, "dayMode");
			glUniform1f(dayNight, 0.0f);

			day = false;
			loadSkyBox();

		}
	}
	if (pressedKeys[GLFW_KEY_P])
	{
		if (pressedKeys[GLFW_KEY_1])
		{
			myBasicShader.useShaderProgram();
			GLint  onPointLightLoc = glGetUniformLocation(myBasicShader.shaderProgram, "onPointLights");
			glUniform1f(onPointLightLoc, 1.0f);
		}
		if (pressedKeys[GLFW_KEY_2])
		{
			myBasicShader.useShaderProgram();
			GLint  onPointLightLoc = glGetUniformLocation(myBasicShader.shaderProgram, "onPointLights");
			glUniform1f(onPointLightLoc, 0.0f);
		}
	}
	if (pressedKeys[GLFW_KEY_F])
	{
		printCameraInfo();
		if (pressedKeys[GLFW_KEY_1])
		{
			myBasicShader.useShaderProgram();
			GLint  flashLightOnLoc = glGetUniformLocation(myBasicShader.shaderProgram, "onFlashLight");
			glUniform1f(flashLightOnLoc, 1.0f);
		}
		if (pressedKeys[GLFW_KEY_2])
		{
			myBasicShader.useShaderProgram();
			GLint  flashLightOnLoc = glGetUniformLocation(myBasicShader.shaderProgram, "onFlashLight");
			glUniform1f(flashLightOnLoc, 0.0f);
		}
	}

	if (pressedKeys[GLFW_KEY_V])
	{
		printCameraInfo();
		if (pressedKeys[GLFW_KEY_1])
		{
			viewMode = 1;
		}
		if (pressedKeys[GLFW_KEY_2])
		{
			viewMode = 2;
		}
		if (pressedKeys[GLFW_KEY_3])
		{
			viewMode = 3;
		}
	}



	if (pressedKeys[GLFW_KEY_K])
	{
		fogDensity = fabs(fogDensity);
		if (fogDensity > 1)
			fogDensity = 1;


		if (pressedKeys[GLFW_KEY_1])
		{
			fogDensity += 0.005;
			fogDensityLoc = glGetUniformLocation(myBasicShader.shaderProgram, "fogDensity");
			glUniform1f(fogDensityLoc, fogDensity);
		}
		if (pressedKeys[GLFW_KEY_2])
		{
			fogDensity -= 0.005;
			fogDensityLoc = glGetUniformLocation(myBasicShader.shaderProgram, "fogDensity");
			glUniform1f(fogDensityLoc, fogDensity);
		}

	}



}

void initOpenGLWindow() {
	myWindow.Create(1024, 768, "OpenGL Project Core");
}

void setWindowCallbacks() {
	glfwSetWindowSizeCallback(myWindow.getWindow(), windowResizeCallback);
	glfwSetKeyCallback(myWindow.getWindow(), keyboardCallback);
	glfwSetCursorPosCallback(myWindow.getWindow(), mouseCallback);
}

void initOpenGLState() {
	glClearColor(0.3, 0.3, 0.3, 1.0);
	//glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
	glViewport(0, 0, myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);
	glEnable(GL_FRAMEBUFFER_SRGB);
	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glEnable(GL_CULL_FACE); // cull face
	glCullFace(GL_BACK); // cull back face
	glFrontFace(GL_CCW); // GL_CCW for counter clock-wise
}

void initObjects() {

	ground.LoadModel("models/road/desert.obj");
	desert.LoadModel("models/sand/sand.obj");
	screenQuad.LoadModel("models/quad/quad.obj");
	StreetLamp.LoadModel("models/StreetLamp/StreetLamp8.obj");
	plane.LoadModel("models/plane/plane2.obj");
	camel.LoadModel("models/camel/camel4.obj");
	tank.LoadModel("models/tank/tank.obj");
	tower.LoadModel("models/tower/dubai.obj");
	heli.LoadModel("models/heli/heli3.obj");
	heli_blade.LoadModel("models/heli/heli_blade.obj");
	cactus.LoadModel("models/cactus/cactus2.obj");
	rock.LoadModel("models/rock/rock.obj");
	road.LoadModel("models/highway/road3.obj");

}



void initShaders() {

	lightShader.loadShader("shaders/lightCube.vert", "shaders/lightCube.frag");
	lightShader.useShaderProgram();
	screenQuadShader.loadShader("shaders/screenQuad.vert", "shaders/screenQuad.frag");
	screenQuadShader.useShaderProgram();

	depthMapShader.loadShader("shaders/depthMapShader.vert", "shaders/depthMapShader.frag");
	depthMapShader.useShaderProgram();

	myBasicShader.loadShader("shaders/basic.vert", "shaders/basic.frag");
	myBasicShader.useShaderProgram();

	groundShader.loadShader("shaders/groundShader.vert", "shaders/groundShader.vert");
	groundShader.useShaderProgram();

}

void initUniforms() {
	myBasicShader.useShaderProgram();

	model = glm::mat4(1.0f);
	modelLoc = glGetUniformLocation(myBasicShader.shaderProgram, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	view = myCamera.getViewMatrix();
	viewLoc = glGetUniformLocation(myBasicShader.shaderProgram, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

	normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
	normalMatrixLoc = glGetUniformLocation(myBasicShader.shaderProgram, "normalMatrix");
	glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));

	projection = glm::perspective(glm::radians(45.0f),
		(float)myWindow.getWindowDimensions().width / (float)myWindow.getWindowDimensions().height,
		0.1f, 1000.0f);
	projectionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	//set the light direction (direction towards the light)
	lightDir = glm::vec3(1.3f, 1.0f, 1.0f);
	lightRotation = glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f));
	lightDirLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightDir");
	glUniform3fv(lightDirLoc, 1, glm::value_ptr(glm::inverseTranspose(glm::mat3(view * lightRotation)) * lightDir));



	lightPositionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "light.position");
	glUniform3f(lightPositionLoc, lightPosition.x, lightPosition.y, lightPosition.z);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light.constant"), 1.0f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light.linear"), 0.09f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light.quadratic"), 0.032f);

	lightPositionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "light1.position");
	glUniform3f(lightPositionLoc, PointLightPosition[1].x, PointLightPosition[1].y, PointLightPosition[1].z);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light1.constant"), 1.0f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light1.linear"), 0.027f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light1.quadratic"), 0.0028f);

	lightPositionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "light2.position");
	glUniform3f(lightPositionLoc, PointLightPosition[2].x, PointLightPosition[2].y, PointLightPosition[2].z);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light2.constant"), 1.0f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light2.linear"), 0.045f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light2.quadratic"), 0.0075f);

	lightPositionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "light3.position");
	glUniform3f(lightPositionLoc, PointLightPosition[3].x, PointLightPosition[3].y, PointLightPosition[3].z);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light3.constant"), 1.0f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light3.linear"), 0.045f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "light3.quadratic"), 0.0075f);


	GLint  lightSpotPositionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.position");
	GLint lightSpotDirLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.direction");
	GLint lightSpotCutOffLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.cutOff");
	GLint lightSpotOuterCutOffLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.outerCutOff");
	GLint  lightSpotAmbientLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.ambient");
	GLint  lightSpotDiffuseLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.Diffuse");
	GLint  lightSpotSpecularLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.Specular");
	glUniform3f(lightSpotPositionLoc, myCamera.getPosition().x, myCamera.getPosition().y, myCamera.getPosition().z);
	glUniform3f(lightSpotDirLoc, myCamera.getFront().x, myCamera.getFront().y, myCamera.getFront().z);
	glUniform1f(lightSpotCutOffLoc, glm::cos(glm::radians(12.5f)));
	glUniform1f(lightSpotOuterCutOffLoc, glm::cos(glm::radians(17.5f)));

	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.constant"), 1.0f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.linear"), 0.027f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.quadratic"), 0.0028f);

	glUniform3f(lightSpotAmbientLoc, 0.01f, 0.01f, 0.01f);
	glUniform3f(lightSpotDiffuseLoc, 1.0f, 1.0f, 1.0f);
	glUniform3f(lightSpotSpecularLoc, 1.0f, 1.0f, 1.0f);


	// tank Spot light

	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.position"), tankPosition.x, 3.0f, tankPosition.z);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.direction"), 0.0f, -0.276836, -0.779571);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.cutOff"), glm::cos(glm::radians(12.5f)));
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.outerCutOff"), glm::cos(glm::radians(17.5f)));
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.constant"), 1.0f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.linear"), 0.027f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.quadratic"), 0.0028f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.ambient"), 0.01f, 0.01f, 0.01f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.Diffuse"), 1.0f, 1.0f, 1.0f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.Specular"), 1.0f, 1.0f, 1.0f);

	//lamp1 Spot light
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.position"), StreetLightPos[0].x, StreetLightPos[0].y, StreetLightPos[0].z);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.direction"), StreetLightDir[0].x, StreetLightDir[0].y, StreetLightDir[0].z);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.cutOff"), glm::cos(glm::radians(36.5f)));
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.outerCutOff"), glm::cos(glm::radians(49.5f)));
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.constant"), 1.0f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.linear"), 0.027f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.quadratic"), 0.0028f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.ambient"), 0.01f, 0.01f, 0.01f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.Diffuse"), 1.0f, 1.0f, 1.0f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.Specular"), 1.0f, 1.0f, 1.0f);

	//lamp2 Spot light
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.position"), StreetLightPos[1].x, StreetLightPos[1].y, StreetLightPos[1].z);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.direction"), StreetLightDir[1].x, StreetLightDir[1].y, StreetLightDir[1].z);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.cutOff"), glm::cos(glm::radians(36.5f)));
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.outerCutOff"), glm::cos(glm::radians(49.5f)));
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.constant"), 1.0f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.linear"), 0.027f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.quadratic"), 0.0028f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.ambient"), 0.01f, 0.01f, 0.01f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.Diffuse"), 1.0f, 1.0f, 1.0f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.Specular"), 1.0f, 1.0f, 1.0f);


	//lamp3 Spot light
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.position"), StreetLightPos[2].x, StreetLightPos[2].y, StreetLightPos[2].z);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.direction"), StreetLightDir[2].x, StreetLightDir[2].y, StreetLightDir[2].z);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.cutOff"), glm::cos(glm::radians(36.5f)));
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.outerCutOff"), glm::cos(glm::radians(49.5f)));
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.constant"), 1.0f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.linear"), 0.027f);
	glUniform1f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.quadratic"), 0.0028f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.ambient"), 0.01f, 0.01f, 0.01f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.Diffuse"), 1.0f, 1.0f, 1.0f);
	glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.Specular"), 1.0f, 1.0f, 1.0f);



	//set light color
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f); //white light
	lightColorLoc = glGetUniformLocation(myBasicShader.shaderProgram, "lightColor");
	glUniform3fv(lightColorLoc, 1, glm::value_ptr(lightColor));

	lightShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(lightShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));


	groundShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(groundShader.shaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));


	myBasicShader.useShaderProgram();
	GLfloat  dayNight = glGetUniformLocation(myBasicShader.shaderProgram, "dayMode");
	glUniform1f(dayNight, 0.0f);


	myBasicShader.useShaderProgram();
	GLint  onPointLight = glGetUniformLocation(myBasicShader.shaderProgram, "onPointLights");
	glUniform1f(onPointLight, 1.0f);

	myBasicShader.useShaderProgram();
	GLint  flashLightOnLoc = glGetUniformLocation(myBasicShader.shaderProgram, "onFlashLight");
	glUniform1f(flashLightOnLoc, 1.0f);

	GLuint fogDensityLoc= glGetUniformLocation(myBasicShader.shaderProgram, "fogDensity");
	glUniform1f(fogDensityLoc, 0.0f);



}

void initFBO() {
	//TODO - Create the FBO, the depth texture and attach the depth texture to the FBO

	//generate FBO id
	glGenFramebuffers(1, &shadowMapFBO);

	//creeam textura pentru FBO
	glGenTextures(1, &depthMapTexture);
	glBindTexture(GL_TEXTURE_2D, depthMapTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

	//atasam textura la FBO
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTexture, 0);


	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);




}

glm::mat4 computeLightSpaceTrMatrix() {

	glm::mat4 lightView = glm::lookAt(glm::mat3(lightRotation) * lightDir, glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	const GLfloat near_plane =-120, far_plane = 120.0f;
	glm::mat4 lightProjection = glm::ortho(-120.0f, 120.0f, -120.0f, 120.0f, near_plane, far_plane);
	glm::mat4 lightSpaceTrMatrix = lightProjection * lightView;

	return lightSpaceTrMatrix;
}




bool depthPass;

void movePlane()
{

	planePosition.x += planeSpeed/2;
	//planePosition.y += 0.0008;
	planePosition.z -= planeSpeed/2;
	//planePosition.w += 0.001;
	int randomHeight = rand() % 5 + 15;

	if (planePosition.x >= 100)
	{
		planePosition = glm::vec4(-100.0f, (float) randomHeight, 100.0f, 135.0f);

	}

}
void moveHeli()
{

	//heliPosition.x += planeSpeed / 2;
	//planePosition.y += 0.0008;
	heliPosition.z += heliSpeed;
	int randomHeight = rand() % 5 + 15;
	int randomX = rand() % 60 - 30;
	bladeRotation += bladeRotationSpeed;

	if (heliPosition.z >= 100)
	{
		heliPosition = glm::vec4((float)randomX,(float) randomHeight, -100.0f, 0.0f);
	}

}

void moveTank()
{

	
	tankPosition.x += 0.0035;
	tankPosition.y += 0.0008;
	tankPosition.z -= 0.1;
	tankPosition.w += 0.001;


	if (tankPosition.z <= -85)
	{

		tankPosition.x = -2.0f;
		tankPosition.y = -2.0f;
		tankPosition.z = 80.0f;
		tankPosition.w = 180.0f;

	}

}

void moveObjects()
{
	moveTank();
	movePlane();
	moveHeli();
}


void drawObj(gps::Model3D& obj,gps::Shader shader, float scaling, float tx, float ty, float tz, float rotate, bool depthPass)
{

	shader.useShaderProgram();


	model = glm::translate(glm::mat4(1.0f), glm::vec3(tx, ty, tz));
	model = glm::rotate(model, glm::radians(rotate), glm::vec3(0.0f, 1.0f, 0.0f));
	model = glm::scale(model, glm::vec3(scaling, scaling, scaling));


	glUniformMatrix4fv(glGetUniformLocation(shader.shaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

	// do not send the normal matrix if we are rendering in the depth map
	if (!depthPass) {
		normalMatrix = glm::mat3(glm::inverseTranspose(view * model));
		glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, glm::value_ptr(normalMatrix));
	}
	obj.Draw(shader);

}



int k = 0;
glm::vec4 pozCactus[20];

glm::vec4 rockPositionsRight[100];
glm::vec4 rockPositionsLeft[100];

void generateRockPositions()
{
	

	for (int i = 0; i < 100; i++)
	{
		int randomX = rand() % 90 + 9;
		int randomY = -2 + rand() % 2 + 6;
		int randomZ = -100+rand() % 200 + 9;

		rockPositionsRight[i].x = randomX;
		rockPositionsRight[i].y = randomY;
		rockPositionsRight[i].z = randomZ;
		rockPositionsRight[i].w = 1 + rand() % 2;
	}

	for (int i = 0; i < 100; i++)
	{
		int randomX = -100 + rand() % 90;
		int randomY = -2 + rand() % 2 + 6;
		int randomZ = -100 + rand() % 200 + 9;

		rockPositionsLeft[i].x = randomX;
		rockPositionsLeft[i].y = randomY;
		rockPositionsLeft[i].z = randomZ;
		rockPositionsLeft[i].w = 1 + rand() % 2;
	}
}

void drawRocks(gps::Shader shader, bool depthPass)
{
	for (int i = 0; i < 100; i++)
	{
		
		drawObj(rock, shader, rockPositionsRight[i].w, rockPositionsRight[i].x, -1.5f, rockPositionsRight[i].z, angleY, depthPass);

		drawObj(rock, shader, rockPositionsLeft[i].w, rockPositionsLeft[i].x, -1.5f, rockPositionsLeft[i].z, angleY, depthPass);
	}

}
void drawCactus(gps::Shader shader, bool depthPass)
{
		for (int i = 5; i <= 11; i++)
		{
			drawObj(cactus, shader, 1, -9.0f, -2.0f, (float)i * 10, angleY, depthPass);

			drawObj(cactus, shader, 1, 8.0f, -2.0f, (float)i * 10, angleY, depthPass);
		}
}

int rot = 1;
void drawObjects(gps::Shader shader, bool depthPass) {


	shader.useShaderProgram();

	drawObj(tank,shader, 1.0f, tankPosition.x, tankPosition.y,tankPosition.z, angleY+tankPosition.w, depthPass);
	drawObj(StreetLamp,shader, 0.5f, -7.0f, -1.0f, 0.0f, angleY, depthPass);
	drawObj(StreetLamp, shader, 0.5f, -3.0f, 0.0f, -50.0f, angleY, depthPass);
	drawObj(StreetLamp, shader, 0.5f, -9.0f, 0.0f, -100.0f, angleY, depthPass);
	drawObj(plane,shader, 1.0f, planePosition.x, planePosition.y + 10, planePosition.z, angleY+planePosition.w, depthPass);

	drawObj(camel,shader, 1, 3.0f, -1.0f, 30.0f, angleY,depthPass);
	drawObj(camel, shader, 1, 5.0f, -1.0f, 35.0f, angleY, depthPass);
	drawObj(camel, shader, 1, 6.0f, -1.0f, 40.0f, angleY, depthPass);

	drawObj(tower, shader,0.1, -60.0f, -2.0f, 40.0f, angleY, depthPass);

	drawObj(road, shader, 3.0f, -2.8f, -4.0f, -155.0f, angleY+90, depthPass);
	drawObj(road, shader, 3.0f, -2.8f, -4.0f, -235.0f, angleY + 90, depthPass);
	drawObj(road, shader, 3.0f, -2.8f, -4.0f, -315.0f, angleY + 90, depthPass);

	drawObj(road, shader, 3.6f, -4.3f, -6.3f, 170.0f, angleY + 85, depthPass);
	drawObj(road, shader, 3.6f, -4.3f-10.0f, -6.3f, 283.0f, angleY + 85, depthPass);
	drawObj(road, shader, 3.6f, -4.3f-20.0f, -6.3f, 396.0f, angleY + 85, depthPass);

	


	drawObj(heli, shader, 0.5f, heliPosition.x, heliPosition.y, heliPosition.z, angleY + heliPosition.w, depthPass);
	drawObj(heli_blade, shader, 0.5f, heliPosition.x, heliPosition.y, heliPosition.z-0.1f, angleY + heliPosition.w+bladeRotation, depthPass);

	drawCactus(shader, depthPass);

	drawRocks(shader, depthPass);



}

void changeViewMode()
{
	switch (viewMode)
	{
	case 1: glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); break;
	case 2:glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); break;
	case 3:glPolygonMode(GL_FRONT_AND_BACK, GL_POINT); break;
	}

}

void renderScene() {

	// depth maps creation pass
	//TODO - Send the light-space transformation matrix to the depth map creation shader and
	//		 render the scene in the depth map
	depthMapShader.useShaderProgram();
	glUniformMatrix4fv(glGetUniformLocation(depthMapShader.shaderProgram, "lightSpaceTrMatrix"), 1, GL_FALSE, glm::value_ptr(computeLightSpaceTrMatrix()));
	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);
	depthPass = true;
	drawObjects(depthMapShader, true);



	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// render depth map on screen - toggled with the M key

	if (showDepth)
	{

		glViewport(0, 0, myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);

		glClear(GL_COLOR_BUFFER_BIT);

		screenQuadShader.useShaderProgram();

		//bind the depth map
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, depthMapTexture);
		glUniform1i(glGetUniformLocation(screenQuadShader.shaderProgram, "depthMap"), 0);

		glDisable(GL_DEPTH_TEST);
		screenQuad.Draw(screenQuadShader);
		glEnable(GL_DEPTH_TEST);

		return;
	}
	else {

		// final scene rendering pass (with shadows)
		depthPass = false;

		glViewport(0, 0, myWindow.getWindowDimensions().width, myWindow.getWindowDimensions().height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		myBasicShader.useShaderProgram();

		view = myCamera.getViewMatrix();
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

		lightRotation = glm::rotate(glm::mat4(1.0f), glm::radians(lightAngle), glm::vec3(0.0f, 1.0f, 0.0f));
		glUniform3fv(lightDirLoc, 1, glm::value_ptr(glm::inverseTranspose(glm::mat3(view * lightRotation)) * lightDir));

		
		glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "light.position"), lightPosition.x, lightPosition.y, lightPosition.z);
		glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "light1.position"), PointLightPosition[1].x, PointLightPosition[1].y, PointLightPosition[1].z);
		glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "light2.position"), PointLightPosition[2].x, PointLightPosition[2].y, PointLightPosition[2].z);
		glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "light3.position"), PointLightPosition[3].x, PointLightPosition[3].y, PointLightPosition[3].z);

		GLint  lightSpotPositionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.position");
		GLint lightSpotDirLoc = glGetUniformLocation(myBasicShader.shaderProgram, "spotLight.direction");
		glUniform3f(lightSpotPositionLoc, myCamera.getPosition().x, myCamera.getPosition().y, myCamera.getPosition().z);
		glUniform3f(lightSpotDirLoc, myCamera.getFront().x, myCamera.getFront().y, myCamera.getFront().z);

		glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp1Light.position"), StreetLightPos[0].x, StreetLightPos[0].y, StreetLightPos[0].z);
		glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp2Light.position"), StreetLightPos[1].x, StreetLightPos[1].y, StreetLightPos[1].z);
		glUniform3f(glGetUniformLocation(myBasicShader.shaderProgram, "streetLamp3Light.position"), StreetLightPos[2].x, StreetLightPos[2].y, StreetLightPos[2].z);


		GLint  TanklightSpotPositionLoc = glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.position");
		GLint TanklightSpotDirLoc = glGetUniformLocation(myBasicShader.shaderProgram, "tankSpotLight.direction");
		glUniform3f(TanklightSpotPositionLoc, tankPosition.x, 3.0f, tankPosition.z);
		glUniform3f(TanklightSpotDirLoc, 0.0f,-0.276836, -0.779571);


		//glUniform3f(TanklightSpotPositionLoc, 0.81, 6.48, -50);
		//glUniform3f(TanklightSpotDirLoc, 0.48f, -0.87, -0.07);

		//bind the shadow map
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, depthMapTexture);
		glUniform1i(glGetUniformLocation(myBasicShader.shaderProgram, "shadowMap"), 3);

		glUniformMatrix4fv(glGetUniformLocation(myBasicShader.shaderProgram, "lightSpaceTrMatrix"),
			1,
			GL_FALSE,
			glm::value_ptr(computeLightSpaceTrMatrix()));

		drawObjects(myBasicShader, false);

		//draw a white cube around the light



		mySkyBox.Draw(skyboxShader, view, projection);


		moveObjects();

	}
	
	changeViewMode();
	drawObj(ground,myBasicShader,0.3f, 0.0f, 0.0f, 0.0f, angleY, false);
	drawObj(desert, myBasicShader, 10.0f, 0.0f, -4.2f, 0.0f, angleY, false);



}

void cleanup() {
	glDeleteTextures(1, &depthMapTexture);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glDeleteFramebuffers(1, &shadowMapFBO);
	myWindow.Delete();
	//cleanup code for your own data
}
int main(int argc, const char* argv[]) {

	try {
		initOpenGLWindow();
	}
	catch (const std::exception & e) {
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	initOpenGLState();
	initObjects();
	initShaders();
	initUniforms();
	initFBO();

	loadSkyBox();

	setWindowCallbacks();

	glCheckError();




	// application loop
	generateRockPositions();
	while (!glfwWindowShouldClose(myWindow.getWindow())) {

		processMovement();
		renderScene();


		glfwPollEvents();
		glfwSwapBuffers(myWindow.getWindow());

		glCheckError();
	}

	//cleanup();

	return 0;
}