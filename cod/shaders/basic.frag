#version 410 core

struct Light{

vec3 position;

float constant;
float linear;
float quadratic;

};

struct SpotLight{

vec3 position;
vec3 direction;

float cutOff;
float outerCutOff;

vec3 ambient;
vec3 diffuse;
vec3 specular;



float constant;
float linear;
float quadratic;

};

#define n 4
uniform Light light;
uniform Light light1;
uniform Light light2;
uniform Light light3;

uniform SpotLight spotLight;
uniform SpotLight tankSpotLight;

uniform SpotLight streetLamp1Light;
uniform SpotLight streetLamp2Light;
uniform SpotLight streetLamp3Light;



uniform float onPointLights;

in vec3 fPosition;
in vec3 fNormal;
in vec2 fTexCoords;

out vec4 fColor;

//matrices
uniform mat4 model;
uniform mat4 view;
uniform mat3 normalMatrix;
//lighting
uniform vec3 lightDir;
uniform vec3 lightColor;
// textures
uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform sampler2D shadowMap;

//components
vec3 ambient;
float ambientStrength = 0.1f;
vec3 diffuse;
vec3 specular;
float specularStrength = 0.5f;

in vec4 fPosEye;

uniform float fogDensity ;
in vec4 fragPosLightSpace;
uniform mat4 lighRotation;


uniform vec3 lightPosition;

uniform float dayMode;
uniform float onFlashLight;

float computeFog()
{

 float fragmentDistance = length(fPosEye);
 
 //float fogg=0.4;
 float fogFactor = exp(-pow(fragmentDistance * fogDensity, 2));
 // float fogFactor = exp(-pow(fragmentDistance * fogg, 2));

 return clamp(fogFactor, 0.0f, 1.0f);
}




vec3 computePointLight(Light light)
{


	vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
    vec3 normalEye = normalize(normalMatrix * fNormal);
	vec3 lightDirN=vec3(normalize(light.position-fPosition));
	vec3 viewDir = normalize(- fPosEye.xyz);
	ambient = ambientStrength * lightColor;
	diffuse = max(dot(normalEye, lightDirN), 0.0f) * lightColor;

	vec3 reflectDir = reflect(-lightDirN, normalEye);
    float specCoeff = pow(max(dot(viewDir, reflectDir), 0.0f), 32);
    specular = specularStrength * specCoeff * lightColor;

	ambient *= vec3(texture(diffuseTexture, fTexCoords));
	diffuse *= vec3(texture(diffuseTexture, fTexCoords));
	specular *= vec3(texture(specularTexture, fTexCoords));

	float distance=length(light.position-fPosition);
	float attenuation=1.0f/(light.constant+light.linear*distance + light.quadratic*(distance * distance));

	ambient*=attenuation;
	diffuse*=attenuation;
	specular*=attenuation;

	//return (ambient+specular+diffuse);
	vec3 color = min((ambient + diffuse) * texture(diffuseTexture, fTexCoords).rgb + specular * texture(specularTexture, fTexCoords).rgb, 1.0f);


	return color;
}

vec3 computeSpotLight(SpotLight light)
{

	vec4 fPosEye = view * model * vec4(fPosition, 1.0f);
    vec3 normalEye = normalize(normalMatrix * fNormal);
	vec3 lightDirN=vec3(normalize(light.position-fPosition));
	vec3 viewDir = normalize(- fPosEye.xyz);


	ambient = light.ambient;
	diffuse = max(dot(normalEye, lightDirN), 0.0f) * lightColor;

	vec3 reflectDir = reflect(-lightDirN, normalEye);
    float specCoeff = pow(max(dot(viewDir, reflectDir), 0.0f), 32);
    specular = light.specular * specCoeff * lightColor;

	ambient *= vec3(texture(diffuseTexture, fTexCoords));
	diffuse *= vec3(texture(diffuseTexture, fTexCoords));
	specular *= vec3(texture(specularTexture, fTexCoords));


	float theta=dot(lightDirN,normalize(-light.direction));
	float epsilon=(light.cutOff-light.outerCutOff); 
	float intensity=clamp((theta-light.outerCutOff)/epsilon,0.0,1.0);
	diffuse *= intensity;
	specular *= intensity;


	float distance=length(light.position-fPosition);
	float attenuation=1.0f/(light.constant+light.linear*distance + light.quadratic*(distance * distance));

	ambient*=attenuation;
	diffuse*=attenuation;
	specular*=attenuation;

	//return (ambient+specular+diffuse);
	vec3 color = min((ambient + diffuse) * texture(diffuseTexture, fTexCoords).rgb + specular * texture(specularTexture, fTexCoords).rgb, 1.0f);


	return color;


}


float computeShadow()
{
float shadow;

//perspective divide
vec3 normalizedCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;

//transform in range [0.1]
normalizedCoords = normalizedCoords * 0.5 + 0.5;

if (normalizedCoords.z > 1.0f)
	return 0.0f;


//closest depth value from light perspective
float closestDepth = texture(shadowMap, normalizedCoords.xy).r;

//adancimea curenta
float currentDepth = normalizedCoords.z;


float bias = 0.005f;
if(currentDepth-bias>closestDepth)
{
	shadow=1.0;

}
else 
{
	shadow=0.0;
}

return shadow;


}




void computeLightComponents()
{		
	vec3 cameraPosEye = vec3(0.0f);//in eye coordinates, the viewer is situated at the origin
	
	//transform normal
	vec3 normalEye = normalize(fNormal);	
	
	//compute light direction
	vec3 lightDirN = normalize(lightDir);
	
	//compute view direction 
	vec3 viewDirN = normalize(cameraPosEye - fPosEye.xyz);
		
	//compute ambient light
	ambient = ambientStrength * lightColor;
	
	//compute diffuse light
	diffuse = max(dot(normalEye, lightDirN), 0.0f) * lightColor;
	
	//compute specular light
	vec3 reflection = reflect(-lightDirN, normalEye);
	float specCoeff = pow(max(dot(viewDirN, reflection), 0.0f), 32);
	specular = specularStrength * specCoeff * lightColor;
}

vec3 turnOnAllStreetLights()
{
	vec3 color=vec3(0.0f);


	color+=computePointLight(light);
	color+=computePointLight(light1);
	color+=computePointLight(light2);
	color+=computePointLight(light3);

	return color;
}

void main() 
{
    //computeDirLight();


	vec3 color=vec3(0.0f);

	float shadow = computeShadow();

	if(dayMode==0)
	{

			if(onPointLights==1)
		{
			color=turnOnAllStreetLights();
		}

	if(onFlashLight==1)
		color+=computeSpotLight(spotLight);


		color+=computeSpotLight(streetLamp1Light);
		color+=computeSpotLight(streetLamp2Light);
		color+=computeSpotLight(streetLamp3Light);
		color+=computeSpotLight(tankSpotLight);


	//color+= min((ambient + (1.0f -shadow)*diffuse) + (1.0f -shadow)*specular, 1.0f);


	}
	else{

		computeLightComponents();
		ambient *= vec3(texture(diffuseTexture, fTexCoords));
		diffuse *= vec3(texture(diffuseTexture, fTexCoords));
		specular *= vec3(texture(specularTexture, fTexCoords));
		

		vec3 daycolor=min((ambient + (1.0f -shadow)*diffuse) + (1.0f -shadow)*specular, 1.0f);
		color=daycolor;

		vec3 baseColor=vec3(0.3f,0.3f,0.3f);
		color = min((ambient ) +(1.0f-shadow)* 0.4*vec3(texture(diffuseTexture, fTexCoords)), 1.0f);
		//color = min((ambient ) +(1.0f-shadow)* diffuse, 1.0f);
	
	}


	//color=daycolor;





	fColor = vec4(color, 1.0f);

	float fogFactor = computeFog();
	vec4 fogColor = vec4(0.5f, 0.5f, 0.5f, 1.0f);
	fColor = mix(fogColor, vec4(color,1.0f), fogFactor);

}
