#version 410 core
layout(location=0) in vec3 vPosition;
layout(location=1) in vec3 vNormal;
//matrices


uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


//lighting

uniform vec3 lightColor;
 vec3 baseColor=vec3(1.0f,1.5f,3.0f);
out vec3 color;
vec3 ambient;
float ambientStrength = 0.2f;


void main()
{

ambient = ambientStrength * lightColor;


color = min(ambient * baseColor, 1.0f);



gl_Position = projection * view * model * vec4(vPosition, 1.0f);




}
