#include "Camera.hpp"

namespace gps {

	//Camera constructor
	Camera::Camera(glm::vec3 cameraPosition, glm::vec3 cameraTarget, glm::vec3 cameraUp) {
		this->cameraPosition = cameraPosition;
		this->cameraTarget = cameraTarget;

		this->cameraFrontDirection = glm::normalize(cameraTarget - cameraPosition);
		this->cameraRightDirection = glm::normalize(glm::cross(this->cameraFrontDirection, cameraUp));
		this->cameraUpDirection = glm::normalize(glm::cross(this->cameraRightDirection, this->cameraFrontDirection));


		//TODO - Update the rest of camera parameters

	}

	//return the view matrix, using the glm::lookAt() function
	glm::mat4 Camera::getViewMatrix() {
		return glm::lookAt(cameraPosition, cameraPosition + cameraFrontDirection, cameraUpDirection);
	}

	//update the camera internal parameters following a camera move event
	void Camera::move(MOVE_DIRECTION direction, float speed) {
		switch (direction) {
		case MOVE_FORWARD:
			this->cameraPosition += cameraFrontDirection * speed;

			break;

		case MOVE_BACKWARD:
			this->cameraPosition -= cameraFrontDirection * speed;

			break;

		case MOVE_RIGHT:
			this->cameraPosition += cameraRightDirection * speed;
			break;

		case MOVE_LEFT:
			this->cameraPosition -= cameraRightDirection * speed;
			break;
		}
	}

	glm::vec3 Camera::getFront()
	{
		return this->cameraFrontDirection;

	}
	glm::vec3 Camera::getPosition()
	{
		return this->cameraPosition;
	}

	//update the camera internal parameters following a camera rotate event
	//yaw - camera rotation around the y axis
	//pitch - camera rotation around the x axis
	void Camera::rotate(float pitch, float yaw) {


		cameraFrontDirection = glm::vec3(
			cos(pitch) * sin(yaw),
			sin(pitch),
			cos(pitch) * cos(yaw)
		);
		cameraRightDirection = glm::normalize(glm::cross(this->cameraFrontDirection, glm::vec3(0.0f, 1.0f, 0.0f)));
		//TODO
	}
}